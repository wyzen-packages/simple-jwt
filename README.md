# Simple JWT

Create/Verify JWT Token

## Changelog

-   Ajout de createDatetimeFromString : creation d'un DateTime depuis une date format String
-   Ajout de removeNullValues : Suppression des champs à NULL d'un tableau

## Tests

-   Require : php 7.4 minimum
-   use docker/composer.sh to use the correct release of php.

## Import

```bash
composer require wyzen-packages/simple-jwt
```

## Usage - Create token

```php
use Wyzen\Php\SimpleJwt;

$jwt = new SimpleJwt([
    'algo' => 'hmac_sha256',
    'exp_interval' => 'PT2S',
    'private_key' => '0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef',
]);
$token = $jwt->createToken([
    'claims1' => 'value1',
    'claims2' => 'value2',
    'claims3' => [
            'data' => 'data claims 3',
        ],
]);
$jwtdata = $token->toString();
dd($jwtdata);
```

## Usage - Check token

```php
use Wyzen\Php\SimpleJwt;

$jwtdata = '....token_jwt....';
$jwt = new SimpleJwt([
    'algo' => 'hmac_sha256',
]);
// Or set to options
$jwt->setPrivateKey('0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef');

/**
 * ret = true or throw RequiredConstraintsViolated exception
 */
$ret = $jwt->check();
```

## Liste des méthodes

-   createToken
-   getOptions
-   setToken
-   setPrivateKey
-   check
-   toString
