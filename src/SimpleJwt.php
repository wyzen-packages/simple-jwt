<?php

declare(strict_types=1);

namespace Wyzen\Php;

use DateInterval;
use DateTime;
use DateTimeImmutable;
use Exception;
use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Signer;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Token\DataSet;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\Plain;
use Lcobucci\JWT\Validation\Constraint\SignedWith;
use Lcobucci\JWT\Validation\RequiredConstraintsViolated;
use Lcobucci\JWT\Validation\Validator;
use Wyzen\Php\Helper;

/**
 * Méthodes JWT: creation/validation
 */
class SimpleJwt
{
    private ?Token $token        = null;
    private string $algo         = 'hmac_sha256';
    private ?string $expInterval = null;
    private ?string $privateKey  = null;
    private ?DataSet $claims     = null;
    private $options = [];

    private const OPTION_KEYS = ['algo', 'exp_interval', 'private_key'];

    public function __construct(array $options = [])
    {
        $this->options     = \array_intersect_key($options, array_fill_keys(self::OPTION_KEYS, null));
        $this->algo        = Helper::getValueEx($options, 'algo', 'hmac_sha256');
        $this->expInterval = Helper::getValueEx($options, 'exp_interval', 'PT2S');
        $this->privateKey  = Helper::getValueEx($options, 'private_key');
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * Creation d'un token
     *
     * @param array $claims
     *
     * @return \Lcobucci\JWT\Token
     */
    public function createToken(array $claims = []): Token
    {
        $signingKey = null;
        $now        = new DateTimeImmutable('now');

        $tokenBuilder = (new Builder(new JoseEncoder(), ChainedFormatter::default()));
        $tokenBuilder = $tokenBuilder->issuedAt($now);

        if (!\is_null($this->privateKey)) {
            $signingKey = InMemory::plainText($this->privateKey);
        }


        if ($this->expInterval) {
            $expireTime = (DateTime::createFromImmutable($now))->add(new DateInterval($this->expInterval));
            $tokenBuilder = $tokenBuilder->expiresAt(DateTimeImmutable::createFromMutable($expireTime));
        }

        /**
         * Fill claims
         */
        foreach ($claims as $name => $value) {
            $tokenBuilder = $tokenBuilder->withClaim($name, $value);
        }

        $this->token = $tokenBuilder->getToken($this->getSigner($this->algo), $signingKey);
        return $this->token;
    }

    /**
     * Retourn le Signer pour le JWT
     *
     * @param string $algo
     *
     * @return Signer
     */
    private function getSigner(string $algo): Signer
    {
        $algorithm = null;
        switch ($algo) {
            case 'sha256':
            case 'hmac_sha256':
                $algorithm = new \Lcobucci\JWT\Signer\Hmac\Sha256();
                break;
            case 'hmac_sha512':
                $algorithm = new \Lcobucci\JWT\Signer\Hmac\Sha512();
                break;
            case 'rsa_sha256':
                $algorithm = new \Lcobucci\JWT\Signer\Rsa\Sha256();
                break;
            case 'rsa_sha512':
                $algorithm = new \Lcobucci\JWT\Signer\Rsa\Sha512();
                break;
            default:
                $algorithm = new \Lcobucci\JWT\Signer\Hmac\Sha256();
                break;
        }
        return $algorithm;
    }


    /**
     * Initialise avec le token
     *
     * @param string $strToken
     *
     * @return DataSet
     */
    public function setToken(string $strToken): DataSet
    {
        $parser = new Parser(new JoseEncoder());
        /** @var Plain $token */
        $this->token  = $parser->parse($strToken);
        $this->claims = $this->token->claims();
        return $this->claims;
    }

    /**
     * Set la private key
     *
     * @param string $privateKey
     *
     * @return void
     */
    public function setPrivateKey(string $privateKey): void
    {
        $this->privateKey = $privateKey;
    }

    /**
     * Test si le token est vérifié
     *
     * @throws Exception
     * @return boolean
     */
    public function check(): bool
    {
        if (\is_null($this->token)) {
            throw new \Exception("Token is null", 501);
        }
        $now = new DateTimeImmutable();

        // Lecture de la clé privée par rapport à la clé public
        $signingKey = InMemory::plainText($this->privateKey);

        $validator = new Validator();
        // Test de la signature
        $validator->assert($this->token, new SignedWith($this->getSigner($this->algo), $signingKey));

        // Test si IAT a expiré
        if ($this->token->claims()->get('exp') && $this->token->isExpired($now)) {
            throw new RequiredConstraintsViolated("Token expired");
            // \assert($this->token->isExpired($now) === false, "Token expired");
        }

        return true;
    }

    /**
     * Retourne le token au format string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->token->toString();
    }

    /**
     * Retourne le token au format string
     *
     * @return string
     */
    public function toString()
    {
        return $this->token->toString();
    }
}
