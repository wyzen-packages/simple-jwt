<?php

namespace Wyzen\Php\Tests;

use Lcobucci\JWT\Validation\RequiredConstraintsViolated;
use PHPUnit\Framework\TestCase;
use Wyzen\Php\SimpleJwt;

class SimpleJwtTest extends TestCase
{
    private $secretKey = '0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef';
    private $claims = [
        'claims1' => 'value1',
        'claims2' => 'value2',
        'claims3' => [
            'data' => 'data claims 3',
        ],
    ];

    private $options1 = [
        'algo' => 'hmac_sha256',
        'exp_interval' => 'PT1S',
        'private_key' => '0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef',
    ];
    private $options1_without_pk = [
        'algo' => 'hmac_sha256',
    ];

    private $bad_options = [
        'algo' => 'hmac_sha256',
        'bad_parameter' => 'PT2S',
        'private_key' => '1234567890',
    ];

    public function testConstruct()
    {
        $this->assertInstanceOf(
            SimpleJwt::class,
            new SimpleJwt()
        );
    }

    public function testCreateTokenWithBadOptions()
    {
        $jwt = new SimpleJwt($this->bad_options);
        $options = $jwt->getOptions();
        $this->assertNotEquals($this->bad_options, $options);
    }

    public function testCreateTokenWithOptions()
    {
        $jwt = new SimpleJwt($this->options1);
        $options = $jwt->getOptions();
        $this->assertEquals($this->options1, $options);
    }

    public function testCreateTokenWithClaims()
    {
        $jwt = new SimpleJwt($this->options1);
        $token = $jwt->createToken($this->claims);

        $jwtdata = $token->toString();

        $this->assertNotNull($jwtdata);
        $this->assertIsString($jwtdata);
    }

    public function testVerifyClaims()
    {
        $jwt = new SimpleJwt($this->options1);

        /** @var Token $token */
        $token = $jwt->createToken($this->claims);

        // Recup des claims d'origine
        $claims = $token->claims();

        $jwtdata = $token->toString();

        // Récup des claims depuis le jwt
        $jwtVerif = new SimpleJwt($this->options1);
        $claimsVerif = $jwtVerif->setToken($jwtdata);

        $this->assertEquals($claims, $claimsVerif);
    }

    public function testVerifySignature()
    {
        $jwt = new SimpleJwt($this->options1);
        $jwt->setPrivateKey($this->secretKey);
        $token = $jwt->createToken($this->claims);

        $jwtdata = $token->toString();

        // Récup des claims depuis le jwt
        $jwtVerif = new SimpleJwt($this->options1_without_pk);
        $jwtVerif->setPrivateKey($this->secretKey);

        $jwtVerif->setToken($jwtdata);

        $result = $jwtVerif->check();

        $this->assertTrue($result);
    }

    public function testVerifyBadSignature()
    {
        $this->expectException(RequiredConstraintsViolated::class);

        $jwt = new SimpleJwt($this->options1);
        $jwt->setPrivateKey($this->secretKey);
        $token = $jwt->createToken($this->claims);

        $jwtdata = $token->toString();

        // Récup des claims depuis le jwt
        $badSecretKey = '0123456789ZZZZZZZ0123456789ZZZZZZZ0123456789ZZZZZZZ0123456789ZZZZZZZ0123456789ZZZZZZZ';
        $jwtVerif = new SimpleJwt($this->options1_without_pk);
        $jwtVerif->setPrivateKey($badSecretKey);

        $jwtVerif->setToken($jwtdata);

        $jwtVerif->check();
    }

    public function testExpiredTimeOk()
    {
        // Create token
        $jwt = new SimpleJwt($this->options1);
        $jwt->setPrivateKey($this->secretKey);
        $token = $jwt->createToken($this->claims);
        $jwtdata = $token->toString();


        // Verif de l'expiration
        $jwtVerif = new SimpleJwt($this->options1_without_pk);
        $jwtVerif->setPrivateKey($this->secretKey);
        $jwtVerif->setToken($jwtdata);

        $result = $jwtVerif->check();

        $this->assertTrue($result);
    }


    public function testExpiredTimeKo()
    {
        $this->expectException(RequiredConstraintsViolated::class);

        // Create token
        $jwt = new SimpleJwt($this->options1);
        $jwt->setPrivateKey($this->secretKey);
        $token = $jwt->createToken($this->claims);
        $jwtdata = $token->toString();


        \sleep(2);

        // Verif de l'expiration
        $jwtVerif = new SimpleJwt($this->options1_without_pk);
        $jwtVerif->setPrivateKey($this->secretKey);
        $jwtVerif->setToken($jwtdata);

        $result = $jwtVerif->check();

        // Never call next code
    }
}
